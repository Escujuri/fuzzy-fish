package com.example.pricechecker.service;

import com.example.pricechecker.domain.Price;
import com.example.pricechecker.domain.response.PriceResponse;
import com.example.pricechecker.repositories.sql.PriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class PriceService {
    @Autowired
    private PriceRepository priceRepository;


    public PriceResponse getPriceResponse(LocalDateTime date, Integer productId, Integer brandId) {
        List<Price> prices;

        if (productId != null && brandId != null) {
            prices = priceRepository.findByStartDateAndProductIdAndBrandId(date, productId, brandId);
        } else if (productId != null) {
            prices = priceRepository.findByStartDateAndProductId(date, productId);
        } else if (brandId != null) {
            prices = priceRepository.findByStartDateAndBrandId(date, brandId);
        } else {
            prices = priceRepository.findByStartDate(date);
        }

        if (!prices.isEmpty()) {
            Price selectedPrice = findHighestPriorityPrice(prices);
            return buildPriceResponse(selectedPrice);
        }

        return null;
    }

    private Price findHighestPriorityPrice(List<Price> prices) {
        return Collections.max(prices, Comparator.comparingInt(Price::getPriority));
    }
    private PriceResponse buildPriceResponse(Price price) {
        PriceResponse priceResponse = new PriceResponse();
        priceResponse.setProductId(price.getProductId());
        priceResponse.setBrandId(price.getBrandId());
        priceResponse.setPriceList(price.getPriceList());
        priceResponse.setStartDate(price.getStartDate());
        priceResponse.setEndDate(price.getEndDate());
        priceResponse.setPrice(price.getPrice());
        priceResponse.setCurrency(price.getCurrency());

        return priceResponse;
    }
}