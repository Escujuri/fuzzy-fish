package com.example.pricechecker.repositories.sql;

import com.example.pricechecker.domain.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface PriceRepository extends JpaRepository<Price, Long> {

    List<Price> findByStartDateAndProductIdAndBrandId(LocalDateTime date,int productId,int brandId);

    List<Price> findByStartDateAndProductId(LocalDateTime date, int productId);

    List<Price> findByStartDateAndBrandId(LocalDateTime date, int brandId);

    List<Price> findByStartDate(LocalDateTime date);
}

