package com.example.pricechecker.controller;

import com.example.pricechecker.domain.Price;
import com.example.pricechecker.domain.response.PriceResponse;
import com.example.pricechecker.service.PriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/prices")
public class PriceController {

    @Autowired
    private PriceService priceService;

    @GetMapping
    public ResponseEntity<PriceResponse> getPrices(
            @RequestParam(value = "startDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime date,
            @RequestParam(value = "productId",required = false) Integer productId,
            @RequestParam(value = "brandId",required = false) Integer brandId) {
        PriceResponse priceResponse = priceService.getPriceResponse(date, productId, brandId);
        if (priceResponse != null) {
            return ResponseEntity.ok(priceResponse);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
