package com.example.pricechecker;


import com.example.pricechecker.controller.PriceController;
import com.example.pricechecker.domain.response.PriceResponse;
import com.example.pricechecker.service.PriceService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@RunWith(MockitoJUnitRunner.class)
public class PriceControllerTest {
    @InjectMocks
    private PriceController priceController;

    @Mock
    private PriceService priceService;

    @Test
    public void testGetPrices_10AM_Day14() {
        LocalDateTime date = LocalDateTime.of(2023, 7, 14, 10, 0);
        int productId = 35455;
        int brandId = 1;
        PriceResponse expectedResponse = createExpectedResponse();

        Mockito.when(priceService.getPriceResponse(date, productId, brandId)).thenReturn(expectedResponse);

        ResponseEntity<PriceResponse> response = priceController.getPrices(date, productId, brandId);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(expectedResponse, response.getBody());
    }

    @Test
    public void testGetPrices_16PM_Day14() {
        LocalDateTime date = LocalDateTime.of(2023, 7, 14, 16, 0);
        int productId = 35455;
        int brandId = 1;
        PriceResponse expectedResponse = createExpectedResponse();

        Mockito.when(priceService.getPriceResponse(date, productId, brandId)).thenReturn(expectedResponse);

        ResponseEntity<PriceResponse> response = priceController.getPrices(date, productId, brandId);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(expectedResponse, response.getBody());
    }

    @Test
    public void testGetPrices_21PM_Day14() {
        LocalDateTime date = LocalDateTime.of(2023, 7, 14, 21, 0);
        int productId = 35455;
        int brandId = 1;
        PriceResponse expectedResponse = createExpectedResponse();

        Mockito.when(priceService.getPriceResponse(date, productId, brandId)).thenReturn(expectedResponse);

        ResponseEntity<PriceResponse> response = priceController.getPrices(date, productId, brandId);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(expectedResponse, response.getBody());
    }

    @Test
    public void testGetPrices_10AM_Day15() {
        LocalDateTime date = LocalDateTime.of(2023, 7, 15, 10, 0);
        int productId = 35455;
        int brandId = 1;
        PriceResponse expectedResponse = createExpectedResponse();

        Mockito.when(priceService.getPriceResponse(date, productId, brandId)).thenReturn(expectedResponse);

        ResponseEntity<PriceResponse> response = priceController.getPrices(date, productId, brandId);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(expectedResponse, response.getBody());
    }

    @Test
    public void testGetPrices_21PM_Day16() {
        LocalDateTime date = LocalDateTime.of(2023, 7, 16, 21, 0);
        int productId = 35455;
        int brandId = 1;
        PriceResponse expectedResponse = createExpectedResponse();

        Mockito.when(priceService.getPriceResponse(date, productId, brandId)).thenReturn(expectedResponse);

        ResponseEntity<PriceResponse> response = priceController.getPrices(date, productId, brandId);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(expectedResponse, response.getBody());
    }

    private PriceResponse createExpectedResponse() {
        PriceResponse expectedResponse = new PriceResponse();
        expectedResponse.setProductId(35455);
        expectedResponse.setBrandId(1);
        expectedResponse.setPriceList(1);
        expectedResponse.setStartDate(LocalDateTime.of(2020, 6, 14, 0, 0, 0));
        expectedResponse.setEndDate(LocalDateTime.of(2020, 12, 31, 23, 59, 59));
        expectedResponse.setPrice(BigDecimal.valueOf(35.50));
        expectedResponse.setCurrency("EUR");
        return expectedResponse;
    }
}
